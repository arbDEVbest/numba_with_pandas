from profile_deco import profile

#@profile
def convert_to_float_negative(data):
    """convert_to_float_negative 'Pompage (MW)' column the negative numbers it's in string we must convert it to negative float
    Args:
        row (DataFrame): Dataframe of eco2mix-regional-tr data
    Returns:
        float: the values converted to negative float
    """
    for idx, row in data.iterrows():
        
        if len(row["Pompage (MW)"].strip("-")) != 0:
            data.loc[idx, "Pompage (MW)"] = float(row["Pompage (MW)"].strip("-")) * -1
        elif row["Pompage (MW)"] == "-":
            data.loc[idx, "Pompage (MW)"] =  0.
        else:
            data.loc[idx, "Pompage (MW)"] = float(row["Pompage (MW)"])

