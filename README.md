# Installation
`python install -r requirements.txt`
# Dataset
J'ai utilisé le jeu de données `eco2mix-regional-tr.csv` pour effectuer un test. La fonction de test était simple : elle convertissait les valeurs numériques négatives sous forme de chaînes de caractères en objets float64 de Numpy.
# Nettoyage de la dataset des valeurs null et la les colonnes vide
La colonne `'Consommation (MW)'` sert de référence pour supprimer les valeurs nulles dans notre processus de nettoyage de données qui est géré par le fichier `get_data.py`.
Nous avons supprimé la colonne `'Column 68'`, car elle était vide.

J'ai implémenté un décorateur `profile` géré par le fichier `profile_deco`, qui utilise le module `cProfile` (c'est une extension C) pour la fonction de base de conversion des cellules de la dataframe .
Le décorateur fourni un ensemble de statistiques décrivant la fréquence et la durée d'exécution des différentes parties du programme. Ces statistiques peuvent être formatées en rapports via le `pstats` module.


> **Warning**
> 
> Étant donné que Numba ne prend pas en charge Pandas l'objet dictionnaire, il ne peut exécuter le code qu'à travers l'interpréteur, ce qui entraîne des frais généraux internes supplémentaires pour Numba.

Pour utiliser numba avec pandas il faut caster les valeurs `pandas.core.series.Series` en `numpy.ndarray` 
> `arr = np.array(data['Pompage (MW)'].copy().reset_index(drop=True))`

## *Le fichier `convert_float_func.py`* 
 contient la fonction de base pour itérer sur la colonne `'Pompage (MW)'`.

## *Le fichier `pandas_apply.py`*
 contient la fonction apply de pandas.

## *Le fichier `numba_parallel.py`*
 contient la fonction de base avec le décorateur numba `@jit` et les arguments `forceobj=True`, `parallel=True`, `fastmath=True`.

> Ces arguments permettent à Numba de mieux optimiser les fonctions qu'il compile en fonction des besoins spécifiques de la fonction et des paramètres qui lui sont passés. Cependant, il est important de comprendre les implications de chaque argument avant de les utiliser, car ils peuvent avoir un impact significatif sur les performances et les résultats des fonctions compilées.

## Pourquoi Utiliser ces arguments

1. `forceobj=True`: cet argument indique à Numba de forcer la création d'un objet Python pour les tableaux Numpy qui sont passés en tant que paramètres dans les fonctions décorées avec jit. Cela peut être utile si la fonction décorée a des effets secondaires qui modifient les tableaux d'origine, car cela garantit que les modifications sont visibles en dehors de la fonction. Cependant, cela peut également entraîner une surcharge de mémoire, car les tableaux doivent être copiés en tant qu'objets Python.

2. `parallel=True`: cet argument permet à Numba d'optimiser les boucles avec une parallélisation automatique en utilisant plusieurs threads ou processeurs. Cela peut améliorer considérablement les performances des fonctions qui effectuent des calculs intensifs dans des boucles.

3. `fastmath=True`: cet argument permet à Numba d'effectuer des opérations mathématiques en utilisant des approximations rapides plutôt que des calculs précis. Cela peut améliorer les performances en échange d'une légère perte de précision. Notez que l'utilisation de cette option peut être appropriée dans certains cas, mais pas dans d'autres où la précision est critique.

### Le fichier `benchmark.py`
Pour lancer les trois fonctions avec les tests de performances

## Exécution
`python benchmark.py`
## Resultat
Avec Numba, on peut observer de meilleures performances, comme on peut le constater sur la figure.
![benchmark test](/data/BEST_time_python_numba.png)
