from numba import jit, njit, prange

@jit(forceobj=True, parallel=True, fastmath=True)
def numba_iter_row(data):
    for idx in prange(len(data)):
        if len(data[idx].strip("-")) != 0:
            data[idx] = float(data[idx].strip("-")) * -1
        elif data[idx] == "-":
            data[idx] = 0.
        else:
            data[idx] = float(data[idx])
            print(data[idx])
    


