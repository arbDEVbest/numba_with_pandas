import pandas as pd
from pathlib import Path

CUR_DIR = Path(__file__).parent
data_dir = CUR_DIR/"data"
eco2mix_data = data_dir/"eco2mix-regional-tr.csv"

def get_data():
    """get_data Get datafarme from csv file, 
    drop null value into 'Consommation (MW)' 
    column and drop 'Column 68' column it's empty column
    Returns:
        DataFrame: return cleaned dataframe
    """

    df = pd.read_csv(eco2mix_data, sep=";")
    df_dropnan = df[pd.notnull(df['Consommation (MW)'])].copy()
    df_dropnan.drop(['Column 68'], axis=1, inplace=True)
    return df_dropnan
