import cProfile, pstats, io


def profile(func):
    """profile A decorator that uses cProfile to Profile a function
    Apply to function with @profile
    Profiles the function using cProfile, and prints out a report
    Adapted from the Python 3.6 docs

    Args:
        func (_type_): A function
    Return:
        inner (_type_): result of function
    """
    def inner(*args, **kwargs):
        profile = cProfile.Profile()
        profile.enable()
        retval = func(*args, **kwargs)
        profile.disable()
        buffer = io.StringIO()
        sortby = "cumulative"
        ps = pstats.Stats(profile, stream=buffer).sort_stats(sortby)
        ps.print_stats()
        print(buffer.getvalue())
        return retval
    return inner
        