import pandas as pd
import numpy as np
from get_data import get_data
from convert_float_func import convert_to_float_negative
from pandas_apply import pandas_apply
from numba_parallel import numba_iter_row
import timeit



loop_iterrow = timeit.repeat("""
convert_to_float_negative(data)
""",
setup="from get_data import get_data; from convert_float_func import convert_to_float_negative; data = get_data() ",
repeat=3, number=1)
print("Pandas iterows best time: ", min(loop_iterrow), "Seconds ✔ OP:", 141905481//min(loop_iterrow))

pd_apply = timeit.repeat(stmt="""
pandas_apply(data)
""",
setup="from get_data import get_data; data = get_data();from pandas_apply import pandas_apply",
repeat=3, number=1)
print("Pandas apply best time: ", min(pd_apply), "Seconds ✔ OP:", 141905481//min(pd_apply))


numba_parallel = timeit.repeat("""
numba_iter_row(arr)
""",
setup="""from get_data import get_data; 
data = get_data(); 
import numpy as np; 
import pandas as pd; 
from numba_parallel import numba_iter_row; 
arr = np.array(data['Pompage (MW)'].copy().reset_index(drop=True))""",
repeat=3, number=1)
                        
print("numba_parallel best time: ", min(numba_parallel), "Seconds ✔ OP:", 141905481//min(numba_parallel))




