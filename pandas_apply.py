

def convert_row(row):
        
        if len(row["Pompage (MW)"].strip("-")) != 0:
            return float(row["Pompage (MW)"].strip("-")) * -1
        elif row["Pompage (MW)"] == "-":
            return 0.
        else:
            return float(row["Pompage (MW)"])


def pandas_apply(data):
    data["Pompage (MW)"] = data.apply(convert_row, axis=1)
    
